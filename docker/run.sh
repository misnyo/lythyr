#!/bin/bash

service postfix start

source /lythyr/env/bin/activate
/lythyr/env/bin/mailman start

tail -f /lythyr/var/logs/mailman.log
