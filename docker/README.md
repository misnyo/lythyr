# Build docker

```bash
$ docker build -t lythyr .
```

# Run docker

```bash
$ docker run -it -p 25:25 lythyr
```

(remote `-it` if you want it in background)


# Get a shell in the system

```bash
$ docker run -it --privileged --entrypoint=/bin/bash lythyr
```
